FROM alpine
MAINTAINER Caroline Glassberg-Powell <caroline@glassberg-powell.com>

RUN apk --update add \
        make \
        perl \
        perl-utils \
        postgresql-client \
    && rm -rf /var/cache/apk/*

RUN cpan TAP::Parser::SourceHandler::pgTAP
ENTRYPOINT ["pg_prove"]
