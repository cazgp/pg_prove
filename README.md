# pg_prove

This is a repo around pg_prove the database testing tool.

## Build

```
podman build -t cazgp/pg_prove .
```

## Run

```
podman run -it --rm cazgp/pg_prove
```

## Push

```
podman login
podman push cazgp/pg_prove
```
